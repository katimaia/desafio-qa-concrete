#encoding: UTF-8
#language: pt

Funcionalidade: Enviar mensagens de texto no Whatsapp
Como um usuário do Whatsapp
Eu gostaria de enviar mensagens de texto para outra pessoa
Para poder conversar

Cenário: Enviar mensagem para um usuário do whatsapp que está na lista de “conversas”
Dado que eu já possua uma conversa na lista de “conversas”
Quando eu seleciono uma conversa
E digito uma mensagem
Então a mensagem digitada aparece acima do campo para digitação

Cenário: Enviar mensagem para um usuário que não está na lista de “conversas”
Dado que eu não possua o usuário na lista de “conversas”
Quando deslizo a tela para a esquerda até a lista “todos”
E seleciono o usuário que possui o whatsapp instalado no celular
E clico sobre o “balão de conversa” ao lado do número do telefone
Então uma nova conversa é iniciada