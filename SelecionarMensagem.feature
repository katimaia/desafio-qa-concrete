#encoding: UTF-8
#language: pt

Funcionalidade: Marcar/Desmarcar mensagens nas conversas do Whatsapp
Como um usuário do Whatsapp
Eu gostaria marcar uma mensagem
De modo que possa visualizá-la depois

Cenário: Marcar mensagem em uma conversa
Dado que já estou em uma conversa
Quando eu pressiono a tela sobre a mensagem
E seleciono a opção “marcar” no menu apresentado
Então uma “estrela” aparece antes do horário da mensagem selecionada

Cenário: Desmarcar mensagem em uma conversa
Dado que eu já tenho uma mensagem marcada
Quando eu pressiono a tela sobre a mensagem marcada
E seleciono a opção “desmarcar” no menu apresentado
Então a “estrela” que estava antes do horário da mensagem desaparecerá

Cenário: Consultar mensagens marcadas
Dado que eu já tenha mensagens marcadas
Quando eu acesso o menu reticências “(...)”
E escolho a opção “dados”
E escolho a opção “mensagens marcadas”
Então as mensagens marcadas irão aparecer em uma lista

Cenário: Desmarcar mensagens pela “lista de mensagens”
Dado que eu já esteja na “lista de mensagens” marcadas
Quando eu pressionar a tela sobre uma mensagem
E selecionar a opção “desmarcar” do menu apresentado
Então a mensagem irá desaparecer da “lista de mensagens”

Cenário: Desmarcar todas as mensagens
Dado que eu já esteja na “lista de mensagens” marcadas
Quando eu acesso o menu reticências “(...)”
E selecionar a opção “desmarcar todas”
E na tela de confirmação da ação selecionar “desmarcar todas”
Então deve apresentar uma mensagem “Nenhuma mensagem marcada”